<?php

namespace App\Http\Controllers;

use App\Models\Trainer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client as oClient;
use Laravel\Passport\RefreshTokenRepository;
use Laravel\Passport\TokenRepository;

class TrainerController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        config(['auth.guards.api.provider' => 'trainers']);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:trainers,email',
            'password' => 'required|min:6'
        ], []);

        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 400);
        }
        Trainer::create($request->only('name', 'email', 'password'));
        auth()->guard('trainer')->attempt($request->only('email', 'password'));

        if (!auth()->guard('trainer')->check()) {
            return response()->json([
                'message' => 'Account created'
            ], 400);
        }
        $tokens = $this->createTokens($request->email, $request->password, null);
        return response()->json([
            'token' => $tokens
        ], 200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ], []);

        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 400);
        }

        auth()->guard('trainer')->attempt($request->only('email', 'password'));

        if (!auth()->guard('trainer')->check()) {
            return response()->json([
                'message' => 'Account created'
            ], 400);
        }
        $tokens = $this->createTokens($request->email, $request->password, null);
        return response()->json([
            'data' => $tokens
        ], 200);
    }

    public function logout()
    {
        $accessToken = auth()->guard($this->guard_trainer)->user()->token();
        $this->revokeAllTokens($accessToken->id);
        $accessToken->revoke();

        return response()->json([
            'message' => 'Logged out successfully'
        ], 200);
    }

    public function details()
    {
        $user = auth()->guard($this->guard_trainer)->user();
        return response()->json([
            'details' => $user
        ], 200);
    }

    public function refreshToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'refresh_token' => 'required'
        ], []);

        if ($validator->fails()) {
            return response()->json(["message" => $validator->errors()], 400);
        }

        return response()->json([
            'data' => $this->getRefreshTokens($request->refresh_token)
        ], 200);
    }

    private function createTokens($email = null, $password = null, $oClientId = null)
    {
        $oClient = $this->getOAuthClient($oClientId);
        $response = Http::asForm()->post($this->site_url . '/oauth/token', [
            'grant_type' => 'password',
            'client_id' => $oClient->id,
            'client_secret' => $oClient->secret,
            'username' => $email,
            'password' => $password,
            'scope' => '*'
        ]);
        return $response->json();
    }

    private function getRefreshTokens($refresh_token)
    {
        $oClient = $this->getOAuthClient();
        $response = Http::asForm()->post($this->site_url . '/oauth/token', [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            'client_id' => $oClient->id,
            'client_secret' => $oClient->secret,
            'scope' => '*',
        ]);
        return $response->json();
    }

    private function getOAuthClient($oClientId = null)
    {
        if (empty($oClientId)) {
            $oClientId = 1;
        }
        return OClient::where(['password_client' => $oClientId, 'provider' => config('auth.guards.api.provider')])->first();
    }

    private function revokeAllTokens($tokenId)
    {
        $tokenRepository = app(TokenRepository::class);
        $refreshTokenRepository = app(RefreshTokenRepository::class);

        $tokenRepository->revokeAccessToken($tokenId);
        $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($tokenId);
    }
}
