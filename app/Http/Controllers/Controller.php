<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $guard_trainer = null;
    protected $guard_client = null;
    protected $site_url = null;

    public function __construct()
    {
        $this->guard_trainer = "trainerApi";
        $this->guard_client = "clientApi";
        $this->site_url = url("/");
    }
}
