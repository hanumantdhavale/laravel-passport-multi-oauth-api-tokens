<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("/trainer/create", [\App\Http\Controllers\TrainerController::class, "register"]);

Route::post("/trainer/login", [\App\Http\Controllers\TrainerController::class, "login"]);

Route::post("/trainer/refresh/token", [\App\Http\Controllers\TrainerController::class, "refreshToken"]);

Route::middleware(['api.auth:trainerApi'])
    ->post("/trainer/logout", [\App\Http\Controllers\TrainerController::class, "logout"]);

Route::middleware(['api.auth:trainerApi'])
    ->get("/trainer/details", [\App\Http\Controllers\TrainerController::class, "details"]);
